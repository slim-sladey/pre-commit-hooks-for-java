FROM amazoncorretto:17-alpine-jdk AS builder

RUN  apk add wget && \
     wget \
       --output-document=checkstyle-all.jar \
       https://github.com/checkstyle/checkstyle/releases/download/checkstyle-10.12.6/checkstyle-10.12.6-all.jar

FROM amazoncorretto:17-alpine-jdk

RUN  mkdir /opt/checkstyle
COPY --from=builder checkstyle-all.jar /opt/checkstyle/
COPY checkstyle.sh /opt/checkstyle/checkstyle.sh
RUN  ln -s /opt/checkstyle/checkstyle.sh /usr/local/bin/

ENTRYPOINT ["checkstyle.sh"]
CMD ["--help"]
