#!/usr/bin/env sh

BASE_DIR=$(dirname $(realpath $0))
REP_FILE=/tmp/$(date +%Y%m%d-%H%M%S)

java -jar "${BASE_DIR}/checkstyle-all.jar" \
     -o "${REP_FILE}" \
     "${@}"
RC=$?

if [ $RC -eq 0 ]; then
  if egrep '(WARN|ERROR)' "${REP_FILE}"; then
    cat "${REP_FILE}"
    exit 1
  fi
fi
exit $RC
