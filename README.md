# [Pre-commit](https://pre-commit.com) hooks for Java

## checkstyle

Add repo and hook along the lines of the following example:
```
- repo: https://gitlab.com/slim-sladey/pre-commit-hooks-for-java
  rev: v0.1.0
  hooks:
  - id: checkstyle
    args: ['-c', 'google_checks.xml']
```
The hook is a very simple wrapper on [checkstyle](https://github.com/checkstyle/checkstyle)